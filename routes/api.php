<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/**
**Basic Routes for a RESTful service:
**Route::get($uri, $callback);
**Route::post($uri, $callback);
**Route::put($uri, $callback);
**Route::delete($uri, $callback);
**
*/
 
 /*
Route::get('products', 'ProductsController@index');
Route::get('products/{product}', 'ProductsController@show');
Route::post('products','ProductsController@store');
Route::put('products/{product}','ProductsController@update');
Route::delete('products/{product}', 'ProductsController@delete');
*/

Route::get('employees', 'EmployeesController@index');
Route::get('employees/{employee}', 'EmployeesController@show');
Route::post('employees','EmployeesController@store');
Route::put('employees/{employee}','EmployeesController@update');
Route::delete('employees/{employee}', 'EmployeesController@delete');

Route::get('companies', 'CompaniesController@index');
Route::get('companies/{company}', 'CompaniesController@show');
Route::post('companies','CompaniesController@store');
Route::put('companies/{company}','CompaniesController@update');
Route::delete('companies/{company}', 'CompaniesController@delete');