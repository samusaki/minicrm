<?php

use Illuminate\Database\Seeder;
use App\Company;
class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        for ($i = 0; $i < 8; $i++) {
           Company::create([
            'name' => $faker->company,
            'logo' => '',
            'email' => $faker->companyEmail,
            'url' => "",
        ]);
        }
    }
}
