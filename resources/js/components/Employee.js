import React from 'react';

/* Stateless component or pure component
 * { product } syntax is the object destructing
 */
const Employee = ({employee}) => {


  //if the props for company is null, return Product doesn't exist
  if(!employee) {

    return(<div><h2>  No Employee was selected </h2> </div>);
  }
    
  //Else, display the company data
  return(  
    <div><h3> Current Employee </h3> 
    <div class="card">
    
    <div class="card-header">{employee.fname} {employee.lname}</div>

    <div class="card-body">
    <h2> </h2>
      <p> {employee.email} </p>
      <p> {employee.phone} </p>
    </div>
    </div>

    </div>
    
     
     
  )
}

export default Employee ;