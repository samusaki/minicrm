import React, { Component } from 'react';
import Employee from './Employee';
class Employees extends Component {
    
    constructor(props) {
        super(props);
           /* Initialize the state. */
           this.state = {
              active: null ,
              employees : []
            }
      }

      componentDidMount() {
        /* fetch API in action */
              
        fetch('/api/employees')
        .then(response => {
            return response.json();
        })
        .then(employees => {
            //Fetched product is stored in the state
            this.setState({ employees });
        });
      }

      handleClick(employee) {
        //handleClick is used to set the state
        this.setState({active:employee});
    }
      renderEmployees() {
       

        return this.state.employees.map(employee => {
            return (
                /* When using list you need to specify a key
                 * attribute that is unique for each list item
                */
               <li className='list-group-item'   key={employee.id} >
                { employee.fname }  { employee.lname } 
                <button type="button" class="btn btn-outline-primary" onClick={
                () =>this.handleClick(employee)} >View</button>
                <button type="button" class="btn btn-outline-primary">Edit</button>
                <button type="button" class="btn btn-outline-primary">Delete</button>
            </li>         
            );
        })
      }
 

      
   render () { 
    return (

        <div className='container'>
          <div className='row'>
            <div className='col-lg-4 col-sm-offset-1'>
                <h3> All Employees </h3>
                  <ul className= "list-group">
                    { this.renderEmployees() }
                  </ul> 
                </div>
                <div className='col-lg-8 col-sm-offset-1'>
                <Employee employee={this.state.active} />
                </div> 
            </div> 
        </div>
   
    );    
  }
}

export default Employees;