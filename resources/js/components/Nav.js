import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Nav extends Component {
    
    constructor(props) {
        super(props);
           /* Initialize the state. */
           this.state = {
              active: null
            }
      }

    _handleClick(menuItem) { 
        this.setState({ active: menuItem });
      }

      
   render () { 
  

    const activeStyle = { color: '#ff3333' };
    const menuItems = [
        {text : "Employees", route: "employees"},
        {text : "Companies", route: "companies"}
    ];
 
    return (

        <nav className="navbar navbar-expand-md navbar-light navbar-laravel">
            <div className="container">
                            <a className="navbar-brand" href='/'>
                                MiniCRM
                            </a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
            {/* Left Side Of Navbar */}
            <ul className="navbar-nav mr-auto">
            {menuItems.map( (menuItem,key) => 
            <li className="nav-item" key={key}>
            <a 
             className={this.state.active === menuItem ? "active" : ""} 
             onClick={this._handleClick.bind(this, menuItem)}
             href={menuItem.route}
            > 
            {menuItem.text}
            </a> </li>
         )}
            </ul>
            {/* Right Side Of Navbar */}
                    <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <a className="nav-link" href="#">Login</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Register</a>
                            </li>
                            <li className="nav-item dropdown">
                                <a id="navbarDropdown" className="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                   UserName <span className="caret"></span>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                </div>
                            </li>
                      
                    </ul>
            </div>
            </div>
       </nav>
   
    );    
  }
}

export default Nav;