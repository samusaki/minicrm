import React, { Component } from 'react';
import {BrowserRouter as Router, Link, Route} from 'react-router-dom';
import ReactDOM from 'react-dom';

import Employees from './Employees';
import Companies from './Companies';
import Nav from './Nav';

/* Main Component */
class Index extends Component {
  constructor() {
  
    super();
    //Initialize the state in the constructor
    this.state = {
        companies: [],
        currentCompany: null
    }
  }
  /*componentDidMount() is a lifecycle method
   * that gets called after the component is rendered
   */
  componentDidMount() {
   
  }

  render() {
    return (
        <Router>
        <div id="app">
        <Route path="/"  component={Nav}/>
        <Route path="/companies" exact component={Companies}/>
        <Route path="/employees" exact component={Employees}/>
    </div>
        </Router>
    );
  }
}
export default Index;
/* The if statement is required so as to Render the component 
 * on pages that have a div with an ID of "root";  
 */ 
if (document.getElementById('root')) {
    ReactDOM.render(<Index />, document.getElementById('root'));
}