import React, { Component } from 'react';
import { FormErrors } from './FormErrors';

class AddEmployee extends Component {

  constructor(props) {
    super(props);
       /* Initialize the state. */
       this.state = {
          newEmployee: {
              fname: '',
              lname: '',
              phone: '',
              email: '',
              company_id: 1,
              formErrors: {email: '', password: ''},
              fnameValid: false,
              lnameValid: false,
              formValid: false
          }
        }
    
    //Boilerplate code for binding methods with `this`
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInput = this.handleInput.bind(this);
  }
  
  /* This method dynamically accepts inputs and stores it in the state */
  handleInput(key, e) {
    
    /*Duplicating and updating the state */
    var state = Object.assign({}, this.state.newEmployee); 
    state[key] = e.target.value;
    this.setState({newEmployee: state });
  }
 /* This method is invoked when submit button is pressed */
  handleSubmit(e) {
    //preventDefault prevents page reload   
    e.preventDefault();
    /*A call back to the onAdd props. The control is handed over
     *to the parent component. The current state is passed 
     *as a param
     */
    this.props.onAdd(this.state.newEmployee);
  }

  render() {
    
    return(
      <div> 
        <div> 
         <h2> Add new Employee </h2>
        {/*when Submit button is pressed, the control is passed to 
         *handleSubmit method 
         */}
        <form onSubmit={this.handleSubmit}>

          <label> 
            Title: 
            {/*On every keystroke, the handeInput method is invoked */}
            <input style={inputStyle} type="text" onChange={(e)=>this.handleInput('title',e)} />
          </label>
          
          <label> 
            Description: 
            <input style={inputStyle}  type="text" onChange={(e)=>this.handleInput('description',e)} />
          </label>
          
          <label>
            Price:
            <input style={inputStyle}  type="number" onChange={(e)=>this.handleInput('price', e)}/>
          </label>

          <input style={inputStyle}  type="submit" value="Submit" />
        </form>
      </div>
    </div>)
  }
}

export default AddEmployee;
  