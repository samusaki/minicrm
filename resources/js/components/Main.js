import React, { Component } from 'react';
import {BrowserRouter as ROuter, Link, Route} from 'react-router-dom';
import ReactDOM from 'react-dom';

import Product from './Product';
import AddProduct from './AddProduct';
import Navigation from './Navigation';

/* Main Component */
class Main extends Component {

  constructor() {
  
    super();
    //Initialize the state in the constructor
    this.state = {
        products: [],
        currentProduct: null
    }
     this.handleAddProduct = this.handleAddProduct.bind(this);
  }
  /*componentDidMount() is a lifecycle method
   * that gets called after the component is rendered
   */
  componentDidMount() {
    /* fetch API in action */
    fetch('/api/products')
        .then(response => {
            return response.json();
        })
        .then(products => {
            //Fetched product is stored in the state
            this.setState({ products });
        });
  }

 renderProducts() {
      
    return this.state.products.map(product => {
        return (
            /* When using list you need to specify a key
             * attribute that is unique for each list item
            */
            <li className='list-group-item'  onClick={
                () =>this.handleClick(product)} key={product.id} >
                { product.title } 
            </li>      
        );
    })
  }

  handleClick(product) {

      //handleClick is used to set the state
      this.setState({currentProduct:product});
  
  }
  
  handleDeleteProduct() {
    
    const currentProduct = this.state.currentProduct;
    fetch( 'api/products/' + this.state.currentProduct.id, 
        { method: 'delete' })
        .then(response => {
          /* Duplicate the array and filter out the item to be deleted */
          var array = this.state.products.filter(function(item) {
          return item !== currentProduct
        });
      
        this.setState({ products: array, currentProduct: null});
   
    });
  }

   handleAddProduct(product) {
    product.price = Number(product.price);
    /*Fetch API for post request */
    fetch( 'api/products/', {
        method:'post',
        /* headers are important*/
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        
        body: JSON.stringify(product)
    })
    .then(response => {
        return response.json();
    })
    .then( data => {
       
        this.setState((prevState)=> ({
            products: prevState.products.concat(data),
            currentProduct : data
        }))
    })
 //update the state of products and currentProduct
  }  
    
  render() {

    return (
        <div id="app">
        <Navigation/> 
        <div className='container'>
          <div className='row'>
         

            <div className='col-lg-4 col-sm-offset-1'>
                <h3> All products </h3>
                  <ul className= "list-group">
                    { this.renderProducts() }
                  </ul> 

            </div> 
            <div className='col-lg-8 col-sm-offset-1'>
           
                <Product product={this.state.currentProduct} />

            </div> 
          
               
          </div>
             <div className='row'>
             <div className='col-lg-12'>
             <AddProduct onAdd={this.handleAddProduct} /> 
             </div>
             </div> 
        </div>
      </div>
   
    );
  }
}

export default Main;

/* The if statement is required so as to Render the component 
 * on pages that have a div with an ID of "root";  
 */ 

if (document.getElementById('root')) {
    ReactDOM.render(<Main />, document.getElementById('root'));
}