import React from 'react';

/* Stateless component or pure component
 * { product } syntax is the object destructing
 */
const Company = ({company}) => {


  //if the props for company is null, return Product doesn't exist
  if(!company) {

    return(<div><h2>  No Company was selected </h2> </div>);
  }
    
  //Else, display the company data
  return(  
    <div> 
      <h2> {Company.name} </h2>
      <p> {Company.email} </p>
      <p>{Company.url}</p>
      <img src={Company.logo}/>
    </div>
  )
}

export default Company ;