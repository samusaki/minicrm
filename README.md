# MiniCRM

Made for SolutionLab as proof of competence

## Setting up the Demo

You need to have all the laravel dependencies and npm 8.12.X to use this demo. First clone the repo or download it as a zip. 

### Laravel Back end
Setup you DB `MySQL 4.8.X`, .env file, take care of migration and seeding. 

`php artisan migrate:fresh`

`php artisan db:seed`
Once you are done with that, run `php artisan serve`. 
The api is accessible at `http://127.0.0.1:8000/api/employees` and `http://127.0.0.1:8000/api/companies`

### React front-end

To make React work, you must run `npm install && npm run dev`. 
Now, head over to `http://127.0.0.1:8000/`