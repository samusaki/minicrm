<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;

class EmployeesController extends Controller
{
    public function index()
	{
	    return Employee::all();
	}

	public function show(Employee $employee)
	{
	    return $employee;
	}

	public function store(Request $request)
    {
		$this->validate($request, [
        //'email' => 'required|unique|max:255',
        'fname' => 'required',
        'lname' => 'required',
        'company_id' => 'exists:companies,id'

    ]);
	    $employee = Employee::create($request->all());

	    return response()->json($employee, 201);
	}

	public function update(Request $request, Employee $employee)
	{
	    $employee->update($request->all());

	    return response()->json($employee, 200);
	}

	public function delete(Employee $employee)
	{
	    $employee->delete();

	    return response()->json(null, 204);
	}
}
